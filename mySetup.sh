#!bin/bash

CURRENT_DIR=${PWD}

cd ../../build

setupATLAS
source */setup.sh

cd ${CURRENT_DIR}
