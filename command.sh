#!bin/bash

########################################################################
OUTPUT_DIR=output/data17_5TeV_lowMu
GRL=GRL/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml


ARR_TRIGGERS=(
    "HLT_noalg_mb_L1J12"
    "HLT_mb_sptrk"
    "HLT_j0_perf_L1RD0_FILLED"
    "HLT_j15"
    "HLT_j25"
    "HLT_j35"
    "HLT_j45"
    "HLT_j55"
    "HLT_j60"
    "HLT_j85"
    "HLT_j100"
    "HLT_j110"
    "HLT_j150"
    "HLT_j175"
    "HLT_j200"
    "HLT_j260"
    "HLT_j300"
    "HLT_j320"
    "HLT_j340" 
    "HLT_j360"
    "HLT_j380"
    "HLT_j400"
    "HLT_j420"
    "HLT_j440"
    "HLT_j450"
    "HLT_j460"
    "HLT_j420_320eta490"
    "HLT_j400_320eta490"
    "HLT_j380_320eta490"
    "HLT_j260_320eta490"
    "HLT_j200_320eta490"
    "HLT_j175_320eta490"
    "HLT_j150_320eta490"
    "HLT_j110_320eta490"
    "HLT_j85_320eta490"
    "HLT_j60_320eta490"
    "HLT_j55_320eta490"
    "HLT_j45_320eta490"
    "HLT_j35_320eta490"
    "HLT_j25_320eta490"
    "HLT_j15_320eta490"
)


########################################################################
# MAIN PART OF THE SCRIPT
#
CURRENT_DIR=${PWD}
mkdir -p ${OUTPUT_DIR}


for TRIGGER in ${ARR_TRIGGERS[*]}
do
	mkdir -p ${OUTPUT_DIR}/${TRIGGER}
	cp ${GRL} ${OUTPUT_DIR}/${TRIGGER}/.
	
	cd ${OUTPUT_DIR}/${TRIGGER}
	
	GRL_BASENAME=$(basename "${GRL}")
	
	#echo ${GRL_BASENAME}
	
	iLumiCalc \
--lumitag=OflLumi-5TeV-003 \
--livetrigger=L1_EM24VHI \
--trigger=${TRIGGER} \
--xml=${GRL_BASENAME} \
--lar \
--lartag=LARBadChannelsOflEventVeto-RUN2-UPD4-08 \
--plots \
--verbose 2>&1 | tee iLumiCalc.log
	cd ${CURRENT_DIR}
done
