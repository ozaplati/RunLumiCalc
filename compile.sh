#!bash/bin

CURRENT_DIR=${PWD}

cd ../../build

asetup Athena,master,latest
cmake ../athena
make

cd ${CURRENT_DIR}
